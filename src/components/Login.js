import { Button, TextField } from '@mui/material';
import React, {useState} from 'react';

import './Login.css';

const Login = () => {
    const [customerID, setCustomerID] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState(false);


    const handleCustomerID = (event) => {
        setCustomerID(event.target.value);
    }

    const handlePassword = (event) => {
        setPassword(event.target.value);
    }

    const login = () => {
        if(customerID === 'test' && password === 'test') {
            setErrorMessage(false);
            console.log("Success");
        } else {
            setErrorMessage(true);
        }
    }

    return (
        <div className="Login">
            <div className="welcome-text">
                Welcome to <br />
                <div className="crystal-delta"> Crystal Delta <br /> </div> 
                e-learning <br />
            </div>
            <div className="login-area">
                <div className='login-title'>
                    Login to your account
                </div>
                <div className='login-input-fields'>
                    <div className='login-input'>
                        <TextField fullWidth label='Customer ID' value={customerID} onChange={handleCustomerID} variant='outlined' margin="dense"/>
                    </div>
                    <div className='login-input'>
                        <TextField fullWidth label='Password' value={password} onChange={handlePassword} type="password" variant='outlined' margin="dense"/>
                    </div>
                </div>
                <div className='login-buttons'>
                    <div className='login-submit'>
                        <Button fullWidth variant="contained" onClick={login}>Login</Button>
                    </div>
                    {errorMessage && <span className="error-message">Login Failed</span>}
                </div>
            </div>
        </div>
    )
}

export default Login