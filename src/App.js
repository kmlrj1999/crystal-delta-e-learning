import logo from './logo.png';
import Login from './components/Login';
import './App.css';

function App() {
  return (
    <div>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <div className='App-blank'></div>
      </header>
      <div className='App-body'>
        <div className='App-body-blank' />
        <Login />
      </div>
      
      
    </div>
  );
}

export default App;
